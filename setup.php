<?php

/**
 * Replace all occurrences of the search string with the replacement string in the specified file
 *
 * @param array|string $search
 * @param array|string $replace
 * @param string       $file File path
 */
function str_replace_in_file( $search, $replace, string $file ): void {
    $fileContent = file_get_contents( $file );
    $fileContent = str_replace( $search, $replace, $fileContent );
    file_put_contents( $file, $fileContent );
}

if ( ! function_exists( 'str_starts_with' ) ) {
    function str_starts_with( $haystack, $needle ): bool {
        return strpos( $haystack, $needle ) === 0;
    }
}

function kebab_case( string $string ): string {
    return strtolower( preg_replace( '%([a-z])([A-Z])%', '\1-\2', $string ) );
}

/**
 * @return array|string|string[]
 */
function camel_case( string $string ) {
    return str_replace( '-', '', ucwords( $string, '-' ) );
}

$projectName       = kebab_case( basename( __DIR__ ) );
$themeDirectory    = __DIR__ . '/web/app/themes/theme-name';
$newThemeDirectory = __DIR__ . '/web/app/themes/' . $projectName;

// Rename theme directory with the project name
if ( file_exists( $themeDirectory ) ) {
    rename( $themeDirectory, $newThemeDirectory );
    $themeDirectory = $newThemeDirectory;
}

$filesToRename = [
    $themeDirectory . '/languages/theme-name.pot',
];
foreach ( $filesToRename as $filename ) {
    if ( ! file_exists( $filename ) ) {
        continue;
    }
    $newFilename = str_replace( 'theme-name', $projectName, $filename );
    rename( $filename, $newFilename );
}

// Replace theme name in files
$files       = [
    "$themeDirectory/style.css",
    "$themeDirectory/assets/js/helpers.js",
    "$themeDirectory/languages/$projectName.pot",
    "$themeDirectory/webpack.mix.js",
    "$themeDirectory/functions.php",
    __DIR__ . '/composer.json',
];
$replacement = [
    '{theme-name}' => $projectName,
    '{ThemeName}'  => camel_case( $projectName ),
];
foreach ( $files as $file ) {
    foreach ( $replacement as $search => $replace ) {
        str_replace_in_file( $search, $replace, $file );
    }
}

// Self destruct
unlink( __DIR__ . '/setup.php' );
