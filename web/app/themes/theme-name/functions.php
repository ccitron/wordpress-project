<?php

require __DIR__ . '/src/helpers.php';

add_action( 'after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     *
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support( 'soil', [
        'clean-up',
        'disable-asset-versioning',
        'disable-trackbacks',
        'jquery-cdn',
        'js-to-footer',
        'nav-walker',
        'nice-search',
        'relative-urls',
    ] );

    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'html5', [ 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ] );
    add_theme_support( 'title-tag' );

    register_nav_menu( 'main', 'Menu principal' );

    load_theme_textdomain( '{theme-name}', __DIR__ . '/languages' );
} );

add_action( 'wp_enqueue_scripts', function () {
    // CSS
    wp_enqueue_style( '{theme-name}', mix( 'app.css', 'dist' ) );

    // JS
    wp_register_script( '{theme-name}-manifest', mix( 'manifest.js', 'dist' ) );
    wp_register_script( '{theme-name}-vendor', mix( 'vendor.js', 'dist' ), [ '{theme-name}-manifest' ] );
    wp_enqueue_script( '{theme-name}', mix( 'app.js', 'dist' ), [ '{theme-name}-manifest', '{theme-name}-vendor' ] );

    wp_localize_script( '{theme-name}', '{ThemeName}', [
        'translations' => [
            //
        ],
    ] );
} );

add_filter( 'style_loader_src', function ( $src ) {
    return str_replace( '/wp/app', '/app', $src );
} );

add_filter( 'script_loader_src', function ( $src ) {
    return str_replace( '/wp/app', '/app', $src );
} );

/*
 * Remove jQuery Migrate
 */
add_action( 'wp_default_scripts', function ( $scripts ) {
    if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) {
        $script = $scripts->registered['jquery'];
        if ( $script->deps ) {
            $script->deps = array_diff( $script->deps, [ 'jquery-migrate' ] );
        }
    }
} );

/*
 * Show only ACF in admin when in development
 */
add_filter( 'acf/settings/show_admin', function () {
    return WP_ENV === 'development';
} );

/*
 * Disable Yoast's Hidden love letter about using the WordPress SEO plugin.
 */
add_action( 'template_redirect', function () {
    if ( is_plugin_active( 'wordpress-seo/wp-seo.php' ) ) {
        $instance = WPSEO_Frontend::get_instance();
        if ( method_exists( $instance, 'debug_mark' ) ) {
            remove_action( 'wpseo_head', [ $instance, 'debug_mark' ], 2 );
        }
    }
}, 9999 );

/*
 * Move Yoast metabox to bottom of admin pages
 */
add_filter( 'wpseo_metabox_prio', function () {
    return 'low';
} );
