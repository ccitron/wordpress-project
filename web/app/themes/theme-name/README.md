# Starting theme Couleur Citron

## Installation

Faire un `npm install` pour initialiser le theme.

### Ajout de packages JS

Faire un `npm install nom_du_package` puis faire dans le fichier JS dedie, par exemple :
```javascript
import Swiper from 'swiper';
```

### Ajout de librairies CSS

Dans le fichier app.css ajouter la ligne, par exemple :
```SCSS
@import '~swiper/dist/idangerous.swiper.css';
```


## Compilation

### En DEV

`npm run watch`

### En PROD

`npm run prod`


## Utilisation du theme

### Grille CSS

#### Utilisation de la grille

Commencer par définir la largeur des marges entre les colonnes en modifiant la valeur de la variable `$largeurCol` dans le fichier [assets/scss/initialize/_variables.scss](assets/scss/initialize/_variables.scss).

Comme pour Bootstrap, les colonnes se gerent via des classes (largeur de colonne et offset) et le responsive est gere pour tablette horizontale avec la classe `m-col-6`, tablette verticale avec la classe `s-col-6` et mobile avec la classe `xs-col-6`.

#### Debug
Pour visualiser la conformite des colonnes en direct sur le front, il est possible de telecharger l'extension Chrome disponible [ici](https://chrome.google.com/webstore/detail/grid-by-couleur-citron/oibadfkgebgnipijjkmdmocbpgopfdhh?hl=fr)

Pour l'utiliser il suffit dans le footer de modifier le code `<div id="get-grid" data-nb="12" data-width="0" data-margin="20"></div>` en editant data-nb pour le nombre de colonnes, data-width pour la largeur max du conteneur (mettre 0 pour un site en full width) et data-margin pour la largeur des marges entre les colonnes.

### Variables SCSS

Ajouter les variables dans le fichier [assets/scss/initialize/_variables.scss](assets/scss/initialize/_variables.scss)

### Gestion des couleurs

Pour la gestion des couleurs, utiliser la fonction SCSS `cc_getColor()`, par exemple : 

```SCSS
background: cc_getColor( social, twitter );
```