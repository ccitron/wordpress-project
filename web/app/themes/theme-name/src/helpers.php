<?php

function mix( $path, $manifestDirectory = '' ): string {
    static $manifests = [];

    if ( ! str_starts_with( $path, '/' ) ) {
        $path = "/{$path}";
    }

    if ( $manifestDirectory && ! str_starts_with( $manifestDirectory, '/' ) ) {
        $manifestDirectory = "/$manifestDirectory";
    }

    $manifestPath = get_theme_file_path( $manifestDirectory . '/mix-manifest.json' );

    if ( ! isset( $manifests[ $manifestPath ] ) ) {
        if ( ! is_file( $manifestPath ) ) {
            throw new Exception( 'The Mix manifest does not exist.' );
        }

        $manifests[ $manifestPath ] = json_decode( file_get_contents( $manifestPath ), true );
    }

    $manifest = $manifests[ $manifestPath ];

    if ( ! isset( $manifest[ $path ] ) ) {
        throw new Exception( "Unable to locate Mix file: {$path}." );
    }

    return get_theme_file_uri( $manifestDirectory . $manifest[ $path ] );
}
