import {HomeController} from './controllers/HomeController';
import {Router} from './Router';

/*
 * Matches a body class with a controller. Classes can be passed via a string or an array.
 * The router stops at the first matched route.
 */
const router = new Router([
  {
    classes: 'home',
    controller: HomeController,
  },
]);
router.start();
